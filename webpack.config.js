const path = require('path');
const webpack = require('webpack');
const AssetsPlugin = require('assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const AssetsByTypePlugin = require('webpack-assets-by-type-plugin');

const CompressionPlugin = require('compression-webpack-plugin');

const assetsPath = './dist/rev-manifest.json';

const UGLIFY_CONF = require('./config').UGLIFY_CONF;
const VENDOR_CONF = require('./config').VENDOR_CONF;


let outputPath = '[name].js';


//write analyser, open browser analyser
const BundleAnalyzerPlugin =
    require('webpack-bundle-analyzer')
        .BundleAnalyzerPlugin;

const Analyzer = () => {
    if (process.env.ANALYZER === 'analyzer') {
        return new BundleAnalyzerPlugin({
            analyzerPort: parseInt(Math.random() * 8000 + 1025),
            logLevel: 'error'
        });
    }
    return () => {}; //do nothing
};

const DEBUG = !(process.env.NODE_ENV === 'production');

if (DEBUG) {
    require('dotenv').config();
} else {
    outputPath = '[name].[hash].js'
}

const modules = () => ({
    rules: [
        {
            test: /\.(jsx|js)$/,
            exclude: [
                path.resolve(__dirname, "node_modules"),
                path.resolve(__dirname, "app/assets")
            ],
            use: 'babel-loader'
        },
        {
            test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        emitFile: false
                    }
                }
            ]
        },
        {
            test: /\.css$/,
            exclude: [
                path.resolve(__dirname, "node_modules"),
                path.resolve(__dirname, "assets")
            ],
            use: ['style-loader', 'css-loader']
        },
        {
            test: /\.(png|jpg|gif)$/,
            use: 'url-loader'
        }
    ]
});

let config = {
    devtool: DEBUG ? 'cheap-module-eval-source-map' : 'nosources-source-map',
    stats: 'verbose',
    entry: {
        app: [
            './app/'
        ],
        vendor: VENDOR_CONF
    },
    resolve: {
        modules: [path.join(__dirname, 'app'), 'node_modules'],
        extensions: ['.js', '.jsx'],
        alias: {
            'styled-components': path.resolve('./node_modules/styled-components')
        }
    },
    output: {
        path: path.join(__dirname, 'dist/public'),
        filename: outputPath
    },
    plugins: [
        new webpack.EnvironmentPlugin(['NODE_ENV', 'API_BASE']),
        new CompressionPlugin({
            test: /\.js$/,
            asset: '[path].gz',
            algorithm: 'gzip'
        }),

        //create the script setting .json
        new AssetsByTypePlugin({path: assetsPath}),

        //copy server side files
        new CopyWebpackPlugin([
            {
                from: 'app',
                to: '../server-build', //outside dist/public
                ignore: ['assets/**/*']
            },
            {
                from: 'app/assets',
                to: 'assets'
            }
        ]),
        Analyzer()
    ],
    module: modules()
};


if (DEBUG) {
    config
        .entry
        .app
        .push('webpack-hot-middleware/client?path=/__webpack_hmr');

  config.plugins = config.plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.js',
      children: true
    })
  ]);
  config.output.publicPath = '/';
} else {
  config.plugins = config.plugins.concat([
    //env is production
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production',
      DEBUG: true
    }),

    //chuncks for vendor with hash in production
    new webpack.optimize.CommonsChunkPlugin({
        name: ['vendor'],
        filename: '[name].[hash].js',
        children: true
    }),

    //uglify the code
    new webpack.optimize.UglifyJsPlugin(UGLIFY_CONF),

    // ignore locale and moment
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  ]);
}

module.exports = config;
