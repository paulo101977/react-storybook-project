
module.exports = {
  VENDOR_CONF: [
    'react',
    'react-router',
    'redux',
    'react-dom',
    'bluebird',
    'humps',
    'history',
    'styled-components',
    'core-js',
    'immutable'
  ],
  UGLIFY_CONF : {
    minimize: true,
    comments: false, // remove comments
    compress: {
      warnings: false, // Suppress uglification warnings
      pure_getters: true,
      conditionals: true,
      sequences: true,
      dead_code: true,
      booleans: true,
      unused: true,
      if_return: true,
      join_vars: true,
      drop_console: true
    },
    mangle: false,
    sourceMap: true, /* fix issue on uglify */
    output: {comments: false}
  }
}
