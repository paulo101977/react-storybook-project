
export const UPDATE_VARIABLE = 'UPDATE_VARIABLE'

export function updateText(text) {
  return {
    type: UPDATE_VARIABLE,
    text
  }
}
