//import { CALL_API, CHAIN_API } from './../middleware/api'

export const CHANGE_PERIOD = Symbol('CHANGE_PERIOD')
export const CHANGE_PLAN = Symbol('CHANGE_PLAN')
export const CHANGE_SPECIAL = Symbol('CHANGE_SPECIAL')

export function updatePeriod(selectedTxt) {
  return {
    type: CHANGE_PERIOD,
    selectedTxt
  }
}

export function updatePlan(selectedTxt) {
  return {
    type: CHANGE_PLAN,
    selectedTxt
  }
}

export function updateSpecial(selectedTxt) {
  return {
    type: CHANGE_SPECIAL,
    selectedTxt
  }
}
