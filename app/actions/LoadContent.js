import { CALL_API, CHAIN_API } from './../middleware/api'

export const LOAD_CONTENT = Symbol('LOAD_CONTENT')
export const LOAD_CONTENT_FAIL = Symbol('LOAD_CONTENT_FAIL')

export function loadContent() {
  return (dispatch, getState) => {
    //const Data = getState().Data

    const url = '/data/content'

    return dispatch({
      [CHAIN_API]: [
          ()=> {
            return {
              [CALL_API]: {
                method: 'get',
                type: 'internal',
                path: url,
                successType: LOAD_CONTENT,
                errorType: LOAD_CONTENT_FAIL
              }
            }
          }
        ]
    })
  }
}
