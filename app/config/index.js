export {default as config} from './config';

let config = {
  API_BASE_URL: process.env.API_BASE_URL || ''
}

export default config;
