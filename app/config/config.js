import {Map} from 'immutable';
import path from 'path';

const URL = process.env.NODE_ENV === 'production' ?
  'http://localhost:8080' : 'http://localhost:3000';

const API_BASE_URL_INTERNAL = process.env.API_BASE_URL_INTERNAL || URL

// the site basename
const API_BASE = process.env.API_BASE || "/"

const baseConfig = {
    all: {
        'API_BASE_URL_EXTERNAL': process.env.API_BASE_URL_EXTERNAL || '',
        'API_BASE_URL_INTERNAL': API_BASE_URL_INTERNAL,
        'API_BASE': API_BASE,
        env: process.env.NODE_ENV || 'development',
        isDev: process.env.NODE_ENV !== 'production',
        basename: process.env.PUBLIC_PATH,
        host: process.env.HOST || 'localhost',
        port: process.env.PORT || 3000, //dev port
        isBrowser: typeof window !== 'undefined',
        isServer: typeof window === 'undefined',
        apiUrl: '',
        publicPath: `${API_BASE}assets`
    },
    test: {},
    development: {},
    production: {
        host: process.env.HOST || 'localhost',
        port: process.env.PORT || 8080,
        apiUrl: ''
    }
};

export default Map().merge(baseConfig.all, baseConfig[baseConfig.all.env]);
