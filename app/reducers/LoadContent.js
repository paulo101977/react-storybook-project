import Immutable from 'immutable'

import { LOAD_CONTENT } from '../actions/LoadContent'


const ContentRedux = Immutable.fromJS({
    content: {
        "header": "Recarga de TV Pré-paga",
        "disableds": [false, true, true],
        "items": [{
            "type": "period",
            "title": "Escolha o período:",
            "items": ["15 dias", "30 dias"]
        }, {
            "type": "plan",
            "title": "Escolha o período:",
            "items": [{
                "title": "Oi TV Start",
                "channel": "127 canais",
                "channelhd": "27 em HD",
                "price": "R$ 27,99",
                "btnmessage": "Lista de canais",
                "selected": "",
                "disabled": true
            }]
        }]
    }
})

let defaultContentState = Immutable.fromJS(ContentRedux)

function appReducer (state = defaultContentState, action) {

	const content  = action.response ? action.response.content : {}

  switch ( action.type ) {
    case LOAD_CONTENT:
      return state.merge({
        content
      })

  	default:
  		return state
	}
}

export default appReducer
