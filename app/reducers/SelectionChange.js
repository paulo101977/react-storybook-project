import Immutable from 'immutable'

import {
	CHANGE_PERIOD,
	CHANGE_PLAN,
	CHANGE_SPECIAL
} from '../actions/SelectionChange'


const SelectionRedux = Immutable.fromJS({
	period: {
		selectedTxt: ''
	},
	plan: {
		selectedTxt: ''
	},
	special: {
		selectedTxt: ''
	}
})

let defaultSelectionState = Immutable.fromJS(SelectionRedux)

function appReducer (state = defaultSelectionState, action) {

  switch ( action.type ) {
    case CHANGE_PERIOD:
      return state.mergeDeep({
        period: {
					selectedTxt: action.selectedTxt
				},
      })
		case CHANGE_PLAN:
			return state.mergeDeep({
				plan: {
					selectedTxt: action.selectedTxt
				},
			})
		case CHANGE_SPECIAL:
			return state.mergeDeep({
				special: {
					selectedTxt: action.selectedTxt
				},
			})
  	default:
  		return state
	}
}

export default appReducer
