import { combineReducers } from 'redux'


import App from './App'
import Update from './Update'
import LoadData from './LoadData'
import SelectionChange from './SelectionChange'
import LoadContent from './LoadContent'

const rootReducer = combineReducers({
  App,
  Update,
  LoadData,
  SelectionChange,
  LoadContent
})

export default rootReducer
