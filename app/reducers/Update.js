import Immutable from 'immutable'

import { UPDATE_VARIABLE } from '../actions/Update'

const TextRedux = Immutable.fromJS({
	text: '',
})

let defaultTextState = Immutable.fromJS(TextRedux)

function appReducer (state = defaultTextState, action) {

  switch ( action.type ) {
    case UPDATE_VARIABLE:
      return state.merge({
        text: action.text,
      })

  	default:
  		return state
	}
}

export default appReducer
