import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute } from 'react-router'
import configureStore from 'store/configureStore'


import App from 'containers/App'
import OiPlay from 'containers/pages/OiPlay'
import RecargaTV from 'containers/pages/RecargaTV'


//get route config
import { config } from '../config'

const basename = config.get("API_BASE")

export default function(history) {
  return (
    <Router history={history}>
        <Route path={basename} component={App} >
          <Route path="oiplay" component={OiPlay} />
        	<IndexRoute component={ RecargaTV } />
        </Route>
    </Router>
  )
}
