import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { useRouterHistory, RouterContext, match } from 'react-router';
import { createMemoryHistory, useQueries } from 'history';
import Promise from 'bluebird';
import configureStore from 'store/configureStore';
import createRoutes from 'Routes';
import { Provider } from 'react-redux';
import Helmet from 'react-helmet';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components'

import path from 'path'
import fs from 'fs'

import config from '../../config/config.js'

//render ejs templates
import ejs from 'ejs'

// Content Security Policy HEADER
import csp from 'node-csp'

let basename = '/'

const options = {
  directives: {
    defaultSrc: ['self'],
    scriptSrc: ['self', 'unsafe-inline'],
    styleSrc: ['self', 'unsafe-inline'],
    fontSrc: ['self', 'data:', 'unsafe-eval', 'unsafe-inline'],
    imgSrc: ['*'],
    frameSrc: ['none'],
    childSrc: ['none'] //deprecated frame-src firefox
  },
  // TODO: nonce works arbitrarily
  // make sure to have unique nonce for each request
  nonce: '614d9122-d5b0-4760-aecf-3a5d17cf0ac9',
  reportOnly: false, // only set reporty only headers
  setAllHeaders: true
}


let scriptSrcs;

let styleSrc;
if ( process.env.NODE_ENV === 'production' ) {
  let refManifest = require('../../../rev-manifest.json');
  scriptSrcs = refManifest.js;
  basename = config.get('API_BASE')
} else {
  scriptSrcs = [
    '/vendor.js',
    '/app.js'
  ];
}

export default (req, res, next)=> {
  let history = useRouterHistory(useQueries(createMemoryHistory))();
  let store = configureStore();
  let routes = createRoutes(history);
  let location = history.createLocation(req.url);
  let template = fs.readFileSync(
    path.resolve(__dirname , '../views/index.ejs'),
    'utf-8'
  )

  match({ routes, location }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      res.redirect(301, redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      res.status(500).send(error.message);
    } else if (renderProps == null) {
      res.status(404).send('Not found');
    } else {
      let [ getCurrentUrl, unsubscribe ] = subscribeUrl();
      let reqUrl = location.pathname + location.search;

      getReduxPromise().then((obj)=> {
        // this is a initial state, dont the server state
        let reduxState = escape(JSON.stringify(store.getState()));

        // handle to styled-components
        const sheet = new ServerStyleSheet();

        //create a reference to compile ejs template
        let templateFn = ejs.compile(template);


        //TODO: Implement feature renderToNodeStream React 16
        let jsx = sheet.collectStyles(
            <Provider store={store}>
                { <RouterContext {...renderProps}/> }
            </Provider>
        )

        //get the page styles
        //const styleTags = sheet.getStyleTags();
        let metaHeader = Helmet.rewind();

        let [ head, tail ]  = templateFn({
            metaHeader,
            //styleTags,
            basename,
            scriptSrcs,
            reduxState,
            styleSrc
        }).split('<!-- html -->')

        //console.log(head, tail)

        // get the stream for the body
        const stream = sheet.interleaveWithNodeStream(
          ReactDOMServer.renderToNodeStream(jsx)
        )

        if ( getCurrentUrl() === reqUrl ) {

          //Add Content Security Policy to response
          if ( process.env.NODE_ENV === 'production' ) {
            csp.add(req, res, options)
          }

          //print the head
          res.write(head)

          // pipe the body
          stream.pipe(res, { end: false })

          // and finalize the response with closing HTML
          stream.on('end', () => res.end(tail))


          // render the template html
          //res.render('index', { metaHeader, html, scriptSrcs, reduxState, styleSrc });
        } else {
          res.redirect(302, getCurrentUrl());
        }
        unsubscribe();
      })
      .catch((err)=> {
        Helmet.rewind();
        unsubscribe();
        next(err);
      });
      function getReduxPromise () {
        let { query, params } = renderProps;
        let fetchData = null;
        let promise = null;
        let comp = renderProps
          .components[renderProps.components.length - 1]
          .WrappedComponent;

        fetchData = comp.fetchData;

        promise = fetchData  ? fetchData({ query, params, store, history }) :
            Promise.resolve();

        return promise;
      }
    }
  });
  function subscribeUrl () {
    let currentUrl = location.pathname + location.search;
    let unsubscribe = history.listen((newLoc)=> {
      if (newLoc.action === 'PUSH' || newLoc.action === 'REPLACE') {
        currentUrl = newLoc.pathname + newLoc.search;
      }
    });
    return [
      () => currentUrl,
      unsubscribe
    ];
  }
};
