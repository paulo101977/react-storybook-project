import Express from 'express'
import path from 'path'
import compression from 'compression'
import clearRequireCacheOnChange from './lib/clearRequireCacheOnChange'
import request from 'request';

import cors from 'cors'

import frameguard  from 'frameguard';


var bodyParser = require('body-parser')

let server = new Express()

const { config } = require('../config')

const port = config.get('port')
const basename = config.get('API_BASE')

// Don't allow ANY frames:
server.use(frameguard({ action: 'deny' }))

//enable cors
server.use(cors())

server.use(compression({threshold : 0}))

// parse application/x-www-form-urlencoded
// server.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
server.use(bodyParser.json())


if (process.env.NODE_ENV === 'production') {
  server.use(basename, Express.static(path.join(__dirname, '../..', 'public')))
} else {
  server.use('/assets', Express.static(path.join(__dirname, '..', 'assets')))

  const webpackDevMiddleware = require('webpack-dev-middleware')
  const webpackHotMiddleware = require('webpack-hot-middleware')
  const webpackConfig = require(path.join(__dirname, '../..', 'webpack.config'))
  const webpack = require('webpack')
  const compiler = webpack(webpackConfig)


  server.use(webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    serverSideRender: true,
    stats: {
      colors: true,
      hash: true,
      timings: true,
      chunks: false
    }
  }))
  server.use(webpackHotMiddleware(compiler, {
    path: '/__webpack_hmr'
  }))

  clearRequireCacheOnChange({ rootDir: path.join(__dirname, '..') })
}

server.set('views', path.join(__dirname, 'views'))
server.set('view engine', 'ejs')


//TODO: change or remove this end-point if necessary
server.get('/data/movie/', (req, res)=> {
  const { serie } = require('../mock/')

  res.json(serie)
})

//TODO: change or remove this end-point if necessary
server.get('/data/content/', (req, res)=> {
  const { content } = require('../mock/')

  res.json(content)
})

server.get('*', (req, res, next)=> {
  require('./middlewares/universalRenderer').default(req, res, next)
})


server.use((err, req, res, next)=> {
  console.log(err.stack)
  // TODO report error here or do some further handlings
  res.status(500).send("something went wrong...")
})


console.log(`Server is listening to port: ${port}`)
server.listen(port)
