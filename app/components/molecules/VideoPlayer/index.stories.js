import React from 'react'
import { storiesOf } from '@storybook/react'
import { VideoPlayer } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

storiesOf('Molecules/VideoPlayer', module)
.add('Normal Case', () => (
	<ThemeProvider theme={ MinhaOi }>
		<VideoPlayer />
	</ThemeProvider>
))
