import React from 'react'
import styled from 'styled-components'

import Title from  './../../atoms/title'
import Input from  './../../atoms/input'
import { Row, Col } from  './../../atoms/container'


const VideoPlayer = (props) => (
 <Col {...props} transparent>
    <video controls>
      <source src={props.src} />
    </video>
 </Col>
);

VideoPlayer.defaultProps = {
  src: "https://www.w3schools.com/html/mov_bbb.mp4"
}

export { VideoPlayer };
