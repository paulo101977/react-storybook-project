import React from 'react'
import styled from 'styled-components'

import * as Title from  './../../atoms/title'

import Img from '../../atoms/img'
import Description from '../../atoms/description'

import { Row, Col } from  './../../atoms/container'

import { CardWrapper } from '../../atoms/cardwrapper'

const Card = (props) => (
  <CardWrapper {...props} transparent padding>
    <Img responsive src={props.src} />
    <Title.H4>{ props.title }</Title.H4>
    <Description>
      {props.description}
    </Description>

    {
      props.hasFooter ?
        <Description>
          {`${props.year} | ${props.genre}`}
        </Description>
        : null
    }
  </CardWrapper>
);

Card.defaultProps = {
  title: '',
  description: '',
  src: '',
  hasFooter: false,
  year: '',
  genre: ''
}

export default Card;
export { Card };
