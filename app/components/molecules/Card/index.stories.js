import React from 'react'
import { storiesOf } from '@storybook/react'
import { Card } from '.'

const src = "./assets/images/bg-hero.png"

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

storiesOf('Molecules/Card', module)
	.add('Normal Case', () => (
		<ThemeProvider theme={ MinhaOi }>
			<Card
				title="Test"
				src={src}
				description="My card for tests"/>
		</ThemeProvider>
	))
