import React from 'react'
import { storiesOf } from '@storybook/react'
import { Accordion } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

import { Col, Row } from '../../atoms/container'
import { Button } from '../../atoms/button'

storiesOf('Molecules/Accordion', module)
	.add('Normal Case', () => (
		<ThemeProvider theme={ MinhaOi }>
			<Accordion id="my-accordion">
				<Row sizeFull transparent>
					<Button>Test 1</Button>
					<Button>Test 2</Button>
				</Row>
			</Accordion>
		</ThemeProvider>
	))
	.add('Disabled Case', () => (
		<ThemeProvider theme={ MinhaOi }>
			<Accordion disabled={true} />
		</ThemeProvider>
	))
