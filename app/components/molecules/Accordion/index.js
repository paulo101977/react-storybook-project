import React from 'react'
import styled, { withTheme } from 'styled-components'


import Icon from '../../atoms/icons'
import Span from '../../atoms/span'

import { Input } from '../../atoms/input'


import { Row, Col } from  './../../atoms/container'
import { AccordionWrapper } from '../../atoms/accordionwrapper'


class AccordionComp extends React.Component{
  constructor(props){
  	super(props);
  	this.state = {
      active: false
    };

  }


  componentDidMount() {
    const { isActive } = this.props;

    this.setState({ active: isActive })

  }

  componentWillReceiveProps(nextProps) {
    const { isActive } = this.props;

    this.setState({ active: isActive })
  }

  __handleClick(event){
    const { disabled } = this.props

    event.preventDefault();

    if(!disabled){
      this.setState({
        active: !this.state.active
      })
    }
  }

  render(){
    const { props } = this;
    const { active } = this.state;
    const { disabled , selected , theme } = props;
    const disabledCls = disabled ? 'disabled' : '';
    const activeCls = active ? "active" : ""

    return (
      <AccordionWrapper
        className={ disabledCls }
        transparent
        padding>

        {/* Accordion Header */}
        <Row
          onClick={(event) => this.__handleClick(event)}
          className={ `accordion-header ${ activeCls }`}
          transparent alignCenter sizeFull spaced>
          <Span>
            { `${props.title}` }
            <Span color={ theme.colors.card }>
            { `${props.selected}` }
            </Span>
          </Span>
          <Icon className="accordion-icon" type="arrow" />
        </Row>

        {/* Accordion Content */}
        <Row
          transparent
          className={ `accordion-content ${ activeCls }`}>
          { props.children }
        </Row>
      </AccordionWrapper>
    )
  }
}

AccordionComp.defaultProps = {
  title: 'Escolha o período:',
  disabled: false,
  selected: '',
  isActive: false
}

const Accordion = withTheme(AccordionComp)

export default Accordion;
export { Accordion };
