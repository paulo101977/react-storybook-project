import React from 'react'
import { storiesOf } from '@storybook/react'
import { CardRechargeTV } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

import Immutable from 'immutable';

const item = Immutable.fromJS({
		title: "My Title",
    disabled: false,
    channel: "My Channel",
    channelhd: "My Channel HD",
    price: "R$ 24,33",
    btnmessage: "Message"
})

const disabledItem = item.merge({
	disabled: true
})

storiesOf('Molecules/CardRechargeTV', module)
	.add('Normal Case', () => (
		<ThemeProvider theme={ MinhaOi }>
			<CardRechargeTV
			  item={item}
			/>
		</ThemeProvider>
	))
	.add('Disabled Case', () => (
		<ThemeProvider theme={ MinhaOi }>
			<CardRechargeTV
				item={disabledItem}/>
		</ThemeProvider>
	))
