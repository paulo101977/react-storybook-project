import React from 'react'
import styled, { withTheme } from 'styled-components'
import Immutable from 'immutable'

import * as Title from  './../../atoms/title'

import Popover from '../Popover'

import { Description } from  '../../atoms/description'

import {
  Row,
  Col
} from  './../../atoms/container'

import { CardRechargeTVWrapper } from '../../atoms/cardrechargewrapper'

import { ButtonUthemed } from '../../atoms/button'


const CardRechargeTVComp = (props) => {

  const {
    theme,
    item
  } = props;

  let title = item.get('title'),
    disabled = item.get('disabled'),
    channel = item.get('channel'),
    channelhd = item.get('channelhd'),
    price = item.get('price'),
    btnmessage = item.get('btnmessage');

  const disabledCls = disabled ? "disabled" : ""

  return (
    <CardRechargeTVWrapper
      className={ disabledCls }
      alignCenter
      transparent
      relative
      {...props}>
      <Col
        alignCenter
        className="card-border"
        spaced
        transparent>
        <Col transparent>
          <Description
            size={-2}
            fontWeight="bold"
            customColor={ theme.colors.card }>
            { title }
          </Description>
          <Description
            size={-2}
            fontWeight="500"
            customColor={ theme.colors.card }>
            { channel }
            </Description>
          <Description
            size={-2}
            fontWeight="500"
            customColor={ theme.colors.card }>
            { channelhd }
          </Description>
        </Col>

        <Description
          size={-3}
          customColor={ theme.colors.dark }>
            { price }
          </Description>
      </Col>

      {
        !disabled ?
          <Col transparent>
            <ButtonUthemed
              line
              size={0}>
                { btnmessage }
            </ButtonUthemed>
          </Col>
          : null
      }
      { /* TODO: make popover with reference to element */ }
      { /* <Popover
            className="popover"
            message="Plano disponível apenas no período de 30 dias."
            selected='' /> */}
    </CardRechargeTVWrapper>
  )
}

CardRechargeTVComp.defaultProps = {
  item: Immutable.fromJS({
    title:'',
    disabled:'',
    channel:'',
    channelhd:'',
    price:'',
    btnmessage :''
  })
}

const CardRechargeTV = withTheme(CardRechargeTVComp)

export default CardRechargeTV;
export { CardRechargeTV };
