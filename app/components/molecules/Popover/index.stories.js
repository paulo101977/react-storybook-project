import React from 'react'
import { storiesOf } from '@storybook/react'
import { Popover } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

import {
  Row,
  Col
} from  './../../atoms/container'

storiesOf('Molecules/PopOver', module)
.add('Normal Case', () => (
	<ThemeProvider theme={ MinhaOi }>
		<Col fillHalf relative>
			<Popover
			  message="Plano disponível apenas no período de 30 dias."
			  selected=''
			/>
		</Col>
	</ThemeProvider>
))
