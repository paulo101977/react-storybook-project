import React from 'react'
import styled, { withTheme } from 'styled-components'

import * as Title from  './../../atoms/title'

import { Description } from  '../../atoms/description'

import {
  Row,
  Col
} from  './../../atoms/container'

import { PopoverWrapper } from '../../atoms/popoverwrapper'

import { ButtonUthemed } from '../../atoms/button'


const Popover = (props) => {
  
  const {
    message,
    disabled
  } = props;

  const disabledCls = disabled ? "disabled" : ""

  return (
    <PopoverWrapper
      className={ disabledCls }
      alignCenter
      transparent
      {...props}>
      <Col alignCenter className="content">
        <Description>{message}</Description>
      </Col>
    </PopoverWrapper>
  )
}

Popover.defaultProps = {
  message: '',
  disabled: false
}


export default Popover;
export { Popover };
