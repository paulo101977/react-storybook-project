import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { List } from 'immutable'

import  Description from '../../atoms/description'
import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'
import { Button } from  '../../atoms/button'

import { Accordion } from '../../molecules/Accordion'
import { CardRechargeTV } from '../../molecules/CardRechargeTV'


//action
import * as SelectionChange from '../../../actions/SelectionChange'

class ContainerSelectionItem extends React.Component{
  constructor(props){
  	super(props);

  }


  renderItem (item, type, index){

    if(item && type){
      switch(type){
        case 'period':
          return <Button
                  key={ `__button__${index}` }
                  onClick={
                    (event) => this.props.updatePeriod(item)}>
                    { item }
                  </Button>
        case 'plan':
          return <CardRechargeTV
                    key={ `__card__${index}` }
                    onClick={
                      (!item.disabled) ?
                        (event) =>
                          this.props.updatePlan(item.get('title'))
                        : null
                    }
                    item={ item } />
        case 'special':
          //TODO: insert special component render here
        default:
          return null;
      }
    }
  }

  render(){
    const {
      item,
      type,
      index
    } = this.props;

    return (
      <Col transparent>
        { this.renderItem(item, type, index) }
      </Col>
    )
  }
}

ContainerSelectionItem.defaultProps = {
  title: '',
  items: List([]),
  updatePeriod: () => {},
  updatePlan: () => {},
  updateSpecial: () => {}
}



export default ContainerSelectionItem

export { ContainerSelectionItem }
