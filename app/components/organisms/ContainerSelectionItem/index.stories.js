import React from 'react'
import { storiesOf } from '@storybook/react'
import { ContainerSelectionItem } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

import Immutable, { Map } from 'immutable'

const item = Immutable.fromJS({
		title:'Oi TV Start',
		channel:'127 canais',
		channel_hd:'27 em HD',
		price:'R$ 27,99',
		btn_message:'Lista de canais',
		selected:''
});

const type = 'plan';
const index = 1;

//Example of how create new stories (storybook)
storiesOf('Organisms/ContainerSelectionItem', module)
	.add('Normal Case', () => (

		<ThemeProvider theme={ MinhaOi }>
      <ContainerSelectionItem
				type={type}
				index={index}
				item={item}/>
    </ThemeProvider>

	))
