import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { List } from 'immutable'

import  Description from '../../atoms/description'
import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'
import { Button } from  '../../atoms/button'

import { Accordion } from '../../molecules/Accordion'
import { CardRechargeTV } from '../../molecules/CardRechargeTV'
import ContainerSelectionItem from '../ContainerSelectionItem'

//action
import * as SelectionChange from '../../../actions/SelectionChange'

class ContainerSelection extends React.Component{
  constructor(props){
  	super(props);
  }

  renderAccordionItem(item){
    const type  = item.get('type');
    const items = item.get('items');

    //propagating methods to components
    const {
      updatePeriod,
      updatePlan,
      updateSpecial
    } = this.props;

    if(typeof items == 'object'){
      return items.map(
        (compItem, index) =>
            <ContainerSelectionItem
              key={`__containeritem__${index}`}
              item={ compItem }
              type={ type }
              index={ index }
              updatePeriod={ updatePeriod }
              updatePlan={ updatePlan }
              updateSpecial={ updateSpecial }/>
        )
    }
  }

  getSelectedText(index){
    const {
      period,
      plan,
      special
    } = this.props;

    switch(index){
      case 0:
        return period.get('selectedTxt')
      case 1:
        return plan.get('selectedTxt')
      case 2:
        return special.get('selectedTxt');
      default:
        return ''
    }
  }

  renderContent (items){

    return items.map(
      (item, index) => {

        if(item && typeof item === 'object'){
          return (
            <Accordion
              key={ `__accordion__${index}` }
              selected={
                this.getSelectedText(index)
              }>
                <Row sizeFull transparent wrap="true">
                  { this.renderAccordionItem(item) }
                </Row>
            </Accordion>
          )
        }
      }
    )
  }

  render(){
    const { title , items } = this.props

    return (
      <Row {...this.props} transparent>
        <Col transparent>
          { this.renderContent(items) }
        </Col>
      </Row>
    )
  }
}


ContainerSelection.defaultProps = {
  title: '',
  items: List([]),
  updatePeriod: () => {},
  updatePlan: () => {},
  updateSpecial: () => {}
}


export default ContainerSelection

export { ContainerSelection }
