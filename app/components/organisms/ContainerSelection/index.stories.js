import React from 'react'
import { storiesOf } from '@storybook/react'
import { ContainerSelection } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

import Immutable, { Map } from 'immutable'


const period = Map({
	selectedTxt: 'test period'
})

const plan = Map({
	selectedTxt: 'test plan'
})

const special = Map({
	selectedTxt: 'test special'
})

const makePlanCards = () => {
	let card = {
		title:'Oi TV Start',
		channel:'127 canais',
		channel_hd:'27 em HD',
		price:'R$ 27,99',
		btn_message:'Lista de canais',
		selected:''
	},
	cards = []

	for(let i = 0; i < 10; i++){
		cards.push(card)
	}

	return cards;
}

const itemsMaker = () => {
	const items =  Immutable.fromJS({
		items:[
    {
      type: 'period',
      title: 'Escolha o período:',
      items: [
				"15 dias",
				"30 dias"
			]
    },
    {
      type: 'plan',
      title: 'Escolha o período:',
      items: makePlanCards()
    },
    {
      type: 'special',
      title: 'Escolha o período:',
      items: []
    }
  ]})

	return items
}

//Example of how create new stories (storybook)
storiesOf('Organisms/ContainerSelection', module)
	.add('Normal Case', () => (

		<ThemeProvider theme={ MinhaOi }>
      <ContainerSelection
				period={period}
				plan={plan}
				special={special}
				items={itemsMaker().get("items")}
				/>
    </ThemeProvider>

	))
