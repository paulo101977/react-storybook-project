import React from 'react'
import { storiesOf } from '@storybook/react'
import { Footer } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

//Example of how create new stories (storybook)
storiesOf('Organisms/Footer', module)
	.add('Footer', () => (
		<ThemeProvider theme={ MinhaOi }>
      <Footer title="My Footer"/>
    </ThemeProvider>
	))
