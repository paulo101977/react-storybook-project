import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'

import { withTheme } from 'styled-components'


const FooterComp = (props) => {
    return (
      <Col sizeFull fillHalf>
        <Row container padding>
          <Title.H2
            color={props.theme.colors.white}>
            {props.title}
          </Title.H2>
        </Row>
      </Col>
    )
  }


FooterComp.defaultProps = {
  title: ''
}

const Footer = withTheme(FooterComp)

export default Footer

export { Footer }
