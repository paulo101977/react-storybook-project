import React from 'react';
import { storiesOf } from '@storybook/react';
import { ContainerCards } from '.';

import { ThemeProvider } from 'styled-components';

import { MinhaOi } from '../../../styles/themes/MinhaOi';

import Immutable, { Map } from 'immutable';

const cardsMaker = () => {
	let cards = []

	for(var i = 0; i < 7; i++){
		let obj = {}
		obj.title = "My title"
		obj.mediumSynopsis = "My description"
		obj.furnitureBaseUrl = "./assets/"
		obj.furniture = [
			{ uri: "images/bg-hero.png" },
			{ uri: "images/bg-hero.png" },
			{ uri: "images/bg-hero.png" },
			{ uri: "images/bg-hero.png" },
		]
		cards.push(obj)
	}

	return Immutable.fromJS(cards);
}

//Example of how create new stories (storybook)
storiesOf('Organisms/ContainerCards', module)
	.add('Normal case', () => (

		<ThemeProvider theme={ MinhaOi }>
      <ContainerCards cards={cardsMaker()}/>
    </ThemeProvider>

	))
