import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Map } from 'immutable'

import  Description from '../../atoms/description'
import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'

import { Card } from '../../molecules/Card'

const renderCards = (cards, hasFooter) =>{
  return cards.map(
    (item, index) => {
      const title = item.get("title")
      const description = item.get("mediumSynopsis")
      const base = item.get("furnitureBaseUrl")
      const urlImg = item.get("furniture").get(3).get("uri")

      return (
        <Card
          title={title}
          description={description}
          src={`${base}${urlImg}`}
          key={`__card__oi__play__${index}`}
          size4
          hasFooter={hasFooter}/>
      )
    }
  )
}

const ContainerCards = (props) => {
  const { title , cards, hasFooter } = props

  return (
    <Row container transparent>
      <Col transparent>
        <Title.H2>{title}</Title.H2>
        <Row wrap="true" transparent negative>
          { renderCards(cards, hasFooter) }
        </Row>
      </Col>
    </Row>
  )
}


ContainerCards.defaultProps = {
  title: ''
}


export default ContainerCards

export { ContainerCards }
