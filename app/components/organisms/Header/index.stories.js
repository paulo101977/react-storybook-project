import React from 'react'
import { storiesOf } from '@storybook/react'
import { Header } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

//Example of how create new stories (storybook)
storiesOf('Organisms/Header', module)
	.add('Header', () => (

		<ThemeProvider theme={ MinhaOi }>
      <Header
				hasLogo
				title="My title"/>
    </ThemeProvider>

	))
	.add('SubHeader', () => (

		<ThemeProvider theme={ MinhaOi }>
      <Header title="My title"/>
    </ThemeProvider>

	))
