import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Icon from '../../atoms/icons'
import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'

import { withTheme } from 'styled-components'



class HeaderComp extends Component {
  constructor(props){
    super(props)
  }


  render() {
    const { title , theme , hasLogo} = this.props

    return (
      <Col {...this.props} sizeFull padding>
        { hasLogo ? <Icon type={"oi-icon"} /> : null}
        <Row container transparent padding>
          <Title.H1
            color={theme.colors.white}>
            { title }
          </Title.H1>
        </Row>
      </Col>
    )
  }
}

HeaderComp.defaultProps = {
  title: '',
  type: 'header',
}

const Header = withTheme(HeaderComp)

export default Header

export { Header }
