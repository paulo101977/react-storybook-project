import React from 'react'
import { storiesOf } from '@storybook/react'
import { ContainerPlayer } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'



//Example of how create new stories (storybook)
storiesOf('Organisms/ContainerPlayer', module)
	.add('Normal case', () => (

		<ThemeProvider theme={ MinhaOi }>
      <ContainerPlayer
					title = 'Video title'
					genre = {["Genre1", "Genre2"]}
					rating = 'Rating'
					episodes =  '12'
					description = 'Custom Description'
					hasSeasons =  {true}
					seasontxt = 'Temporada TXT'
					hasWatchLater = {true}
					watchLaterTxt = 'Assistir Depois'
					watchTraillerTxt = 'Assistir Trailler'/>
    </ThemeProvider>

	))
