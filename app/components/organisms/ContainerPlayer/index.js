import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import  Description from '../../atoms/description'
import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'
import { StyledLink } from '../../atoms/link'

import { VideoPlayer } from '../../molecules/VideoPlayer'

import { withTheme } from 'styled-components'


class ContainerPlayerComp extends Component {
  constructor(props){
    super(props)
  }

  parseGenre(){
    const { genre } = this.props;
    let genreArr = []

    if(genre){
      //return genre.join(', ')
      genre.map((item)=>{
        genreArr.push(item)
      })
    }

    return genreArr.join(', ')

  }

  renderContent(){
    const {
      title,
      genre,
      rating,
      episodes,
      description,
      hasSeasons,
      seasontxt
    } = this.props;

    return (
      <Col size2 transparent>
        <Title.H2>
          { title }
        </Title.H2>
        <Description margin>
          { description }
        </Description>
        <Description margin>
          <span>
            Gênero: <b>{ this.parseGenre() }</b>
          </span>
          <span> | Classificação: <b>{ rating }</b> </span>
          { episodes ? ` | Episódios: ${ episodes }` : null }
        </Description>
        {
          hasSeasons ?
            <Description margin>
              <StyledLink decoration>
                { seasontxt }
              </StyledLink>
            </Description> : null
        }
      </Col>
    )
  }

  render() {
    const {
      title,
      theme,
      description,
      hasWatchLater,
      watchLaterTxt,
      watchTraillerTxt
    } = this.props

    return (
      <Col transparent padding>
        <Row transparent container>
          { this.renderContent() }

          {/* video player wrapper */}
          <Col size2 transparent>
            <VideoPlayer/>

            {/* watch and trailler link */}
            {
              hasWatchLater ?
                <Description margin>
                  <StyledLink>
                    { watchLaterTxt }
                  </StyledLink> | <StyledLink>
                    { watchTraillerTxt }
                  </StyledLink>
                </Description> : null
            }
          </Col>
        </Row>
      </Col>
    )
  }
}

ContainerPlayerComp.defaultProps = {
  title: '',
  genre: '',
  rating: '',
  episodes: '',
  description: '',
  hasSeasons: true,
  seasontxt: '',
  hasWatchLater: true,
  watchLaterTxt: '',
  watchTraillerTxt: ''
}

const ContainerPlayer = withTheme(ContainerPlayerComp)

export default ContainerPlayer

export { ContainerPlayer }
