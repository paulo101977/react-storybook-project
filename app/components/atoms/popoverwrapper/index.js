import React, { Component } from 'react'
import styled from 'styled-components'

import { Col } from '../container'

const PopoverWrapper = Col.extend`
		width: 100%;
		width: 200px;
    position: absolute;
    top: calc(35% - 34px);
    height: 78px;
    background: white;
    align-self: center;
    justify-self: center;
		box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.3);

		p{
			font-size: 0.7rem;
		}

		.content{
			width: 100%;
			height: 100%;
			background: white;
			justify-content: center;
		}

		/* arrow */
		&:after{
			background: white;
	    content: '';
			height: 20px;
	    width: 20px;
			position: absolute;
			top: calc(68px);
	    transform: rotate(45deg);
			box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.3);
		}
`

export default PopoverWrapper;
export { PopoverWrapper };
