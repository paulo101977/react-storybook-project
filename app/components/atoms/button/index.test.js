import React from 'react';
import { mount } from 'enzyme';
import { Button } from './index';
import sinon from 'sinon';


import { ThemeProvider, withTheme } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

// mock class to text state change on click button
class MockTest extends React.Component {
    constructor(props){
    	super(props);
    	this.state = {
        click: 0
      };
    }

    onClickEvent(event){
      event.preventDefault();

      let { click } = this.state;

      this.setState({ click: ++click })
    }

    render() {
        const { click } = this.state;
        return (
            <div className="container">
              <ThemeProvider theme={ MinhaOi }>
                <Button
                  className="my-test-class"
                  onClick={
                    (event) => this.onClickEvent(event)

                  }>
                    { `${this.props.children}${click}` }
                </Button>
              </ThemeProvider>
            </div>
        );
    }
}

export const tests = describe('Button', () => {
  it('should have the text "Sample Button text.', () => {
    const output = mount(
      <ThemeProvider theme={ MinhaOi }>
        <Button>Sample Button text.</Button>
      </ThemeProvider>
    );
    expect(output.text()).toContain('Sample Button text.');
  });

  it('should have clicked at button 3 times', () => {
    const onButtonClick = sinon.spy();
    const output = mount(
      <ThemeProvider theme={ MinhaOi }>
        <Button onClick={onButtonClick}>
          Sample Button text.
        </Button>
      </ThemeProvider>
    );

    //click 3 times
    const btn = output.find('button');
    btn.simulate('click')
    btn.simulate('click')
    btn.simulate('click')


    //expected call 3 times the button event
    expect(onButtonClick.calledThrice).toBe(true);
  });

  it('should have clicked at button an call the function', () => {
    const output = mount(
      <MockTest>
        Sample Button text
      </MockTest>
    );

    //test for the check if state is the same, before call click
    expect(output.text()).toContain('Sample Button text0');

    //click 4 times
    const btn = output.find('button');
    btn.simulate('click')
    btn.simulate('click')
    btn.simulate('click')
    btn.simulate('click')

    //get the state
    let { click } = output.state()

    //expected call 4 times the button event
    expect(click).toBe(4);

    //check if state is different, after call click button
    expect(output.text()).toContain('Sample Button text4');
  });
});
