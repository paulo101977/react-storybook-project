import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withInfo} from '@storybook/addon-info';
import {specs} from 'storybook-addon-specifications';
import {tests} from './index.test.js';

import Button from '.'
import { ButtonLink } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

import { Col, Row } from './../container'

storiesOf('Atoms/Button', module)
.add('Normal Case', withInfo('Sample Button elements')(
	() => (
		<ThemeProvider theme={ MinhaOi }>
			<Button className='Teste'>Normal</Button>
		</ThemeProvider>
	))
)
.add('primary Case', withInfo('Sample Button elements')(
	() => (
		<ThemeProvider theme={ MinhaOi }>
			<Button primary>primary</Button>
		</ThemeProvider>
	))
)
.add('info Case', withInfo('Sample Button elements')(
	() => (
		<ThemeProvider theme={ MinhaOi }>
			<Button info>info</Button>
		</ThemeProvider>
	))
)
.add('All Cases', withInfo('Sample Button elements')(
	() => (
		<ThemeProvider theme={ MinhaOi }>
			<div>
		 		<Button>Normal</Button>
		 		<Button primary full>primary</Button>
		 		<ButtonLink primary >ButtonLink</ButtonLink>
		 	</div>
		</ThemeProvider>
	))
)
.add('Col Cases',withInfo('Sample Button elements')(
	() => (
		<ThemeProvider theme={ MinhaOi }>
			<Col>
		 		<Button>Normal</Button>
		 		<Button primary full>primary</Button>
		 	</Col>
		</ThemeProvider>
	))
)
.add('Row Cases', withInfo('Sample Button elements')( 
	() => (
		<ThemeProvider theme={ MinhaOi }>
			<Row>
		 		<Button>Normal</Button>
		 		<Button primary full>primary</Button>
		 	</Row>
		</ThemeProvider>
	))
);
