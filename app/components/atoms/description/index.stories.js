import React from 'react'
import { storiesOf } from '@storybook/react'
import Description from '.'

storiesOf('Atoms/Description', module)
.add('Normal Case', () => (
  <div>
        <Description> Text is Normal size 1</Description>
        <br />
        <Description size={2}> Text is Normal size 2</Description>
        <br />
        <Description size={3}> Text is Normal size 3</Description>
        <br />
        <Description size={2}> Text is Normal size 3</Description>
        <br />
        <Description size={1}> Text is Normal size 3</Description>
        <br />
        <Description size={-1}> Text is Normal size 3</Description>
        <br />
        <Description size={-2}> Text is Normal size 3</Description>
    </div>
))
.add('Upper Case', () => (
    <div>
        <Description isUpperCase  > Text is UpperCase size 1</Description>
        <br />
        <Description isUpperCase  size={2}> Text is UpperCase size 2</Description>
        <br />
        <Description isUpperCase  size={3}> Text is UpperCase size 3</Description>
    </div>
))
.add('Lower Case', () => (
    <div>
        <Description isLowerCase  > Text is LowerCase size 1</Description>
        <br />
        <Description isLowerCase  size={4} > Text is LowerCase size 4=1</Description>
        <br />
        <Description isLowerCase  size={2}> Text is LowerCase size 2</Description>
        <br />
        <Description isLowerCase  size={3}> Text is LowerCase size 3</Description>
    </div>
))
.add('Font scale Size 1', () => (
  <Description> Text Size 1 </Description>
))
.add('Font scale Size 2', () => (
  <Description size={2} > Text Size 2</Description>
))
.add('Font scale Size 3', () => (
  <Description size={3}  > Text Size 3</Description>
)) ;
