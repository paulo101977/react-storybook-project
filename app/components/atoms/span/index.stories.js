import React from 'react'
import { storiesOf } from '@storybook/react'
import Label from '.'

storiesOf('Atoms/Label', module)
.add('Normal Case', () => (
  <div>
        <Label> Text is Normal size 1</Label>
        <br />
        <Label size={2}> Text is Normal size 2</Label>
        <br />
        <Label size={3}> Text is Normal size 3</Label>
        <br />
        <Label size={2}> Text is Normal size 3</Label>
        <br />
        <Label size={1}> Text is Normal size 3</Label>
        <br />
        <Label size={-1}> Text is Normal size 3</Label>
        <br />
        <Label size={-2}> Text is Normal size 3</Label>
    </div>
))
.add('Upper Case', () => (
    <div>
        <Label isUpperCase  > Text is UpperCase size 1</Label>
        <br />
        <Label isUpperCase  size={2}> Text is UpperCase size 2</Label>
        <br />
        <Label isUpperCase  size={3}> Text is UpperCase size 3</Label>
    </div>
))
.add('Lower Case', () => (
    <div>
        <Label isLowerCase  > Text is LowerCase size 1</Label>
        <br />
        <Label isLowerCase  size={4} > Text is LowerCase size 4=1</Label>
        <br />
        <Label isLowerCase  size={2}> Text is LowerCase size 2</Label>
        <br />
        <Label isLowerCase  size={3}> Text is LowerCase size 3</Label>
    </div>
))
.add('Font scale Size 1', () => (
  <Label> Text Size 1 </Label>
))
.add('Font scale Size 2', () => (
  <Label size={2} > Text Size 2</Label>
))
.add('Font scale Size 3', () => (
  <Label size={3}  > Text Size 3</Label>
)) ;
