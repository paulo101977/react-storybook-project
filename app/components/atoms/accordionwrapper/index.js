import React, { Component } from 'react'
import styled from 'styled-components'
import { Col } from '../container'


const AccordionWrapper = Col.extend`
		transition: max-height 300ms;

		&.disabled{
			opacity: 0.5;
		}

    .accordion-header{

      &  span  svg{
        transition: transform 300ms;
      }

      &.active{
        &  span svg{
          transform: rotate(180deg);
        }
      }
    }

  .accordion-content{
    overflow: hidden;
		max-height: 0;
		opacity: 0;
    transform: scaleY(0);
    transform-origin: top;
    transition: transform 0.5s, max-height 0.5s, opacity 0.5s;

    &.active{
      transform: scaleY(1);
			max-height: 2000px;
			opacity: 1;
    }
  }
`

export default AccordionWrapper;

export { AccordionWrapper };
