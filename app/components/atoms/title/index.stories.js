import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withInfo} from '@storybook/addon-info';
import {specs} from 'storybook-addon-specifications';
import {tests} from './index.test.js';
import  * as Title from '.'

storiesOf('Atoms/Title', module)
.add('Normal Case', withInfo('Sample H elements')(
  () =>
      <div>
          <Title.H1> Text is Normal  h1</Title.H1>
          <Title.H2> Text is Normal  h2</Title.H2>
          <Title.H3> Text is Normal  h3</Title.H3>
          <Title.H4> Text is Normal  h4</Title.H4>
          <Title.H5> Text is Normal  h5</Title.H5>
          <Title.H6> Text is Normal  h6</Title.H6>
      </div>
  ))
  .add('isUpperCase Case', withInfo('Sample H elements')(
  () => (
    <div>
        <Title.H1 isUpperCase> Text is Normal  h1</Title.H1>
        <Title.H2 isUpperCase> Text is Normal  h2</Title.H2>
        <Title.H3 isUpperCase> Text is Normal  h3</Title.H3>
        <Title.H4 isUpperCase> Text is Normal  h4</Title.H4>
        <Title.H5 isUpperCase> Text is Normal  h5</Title.H5>
        <Title.H6 isUpperCase> Text is Normal  h6</Title.H6>
    </div>
)))
.add('isLowerCase Case', withInfo('Sample H elements')(
  () => (
    <div>
        <Title.H1 isLowerCase> Text is Normal  h1</Title.H1>
        <Title.H2 isLowerCase> Text is Normal  h2</Title.H2>
        <Title.H3 isLowerCase> Text is Normal  h3</Title.H3>
        <Title.H4 isLowerCase> Text is Normal  h4</Title.H4>
        <Title.H5 isLowerCase> Text is Normal  h5</Title.H5>
        <Title.H6 isLowerCase> Text is Normal  h6</Title.H6>
    </div>
)));
