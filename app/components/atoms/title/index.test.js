import React from 'react';
import {mount} from 'enzyme';
import * as Title from './index';

export const tests = describe('Title', () => {
  it('should have the text "Sample Title text.', () => {
    const output = mount(<Title.H1>Sample Title text.</Title.H1>);
    expect(output.text()).toContain('Sample Title text.');
  });
});
