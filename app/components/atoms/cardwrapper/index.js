import React, { Component } from 'react'
import styled from 'styled-components'

import { Col } from '../container'

const CardWrapper = Col.extend`
  transition: color 300ms;

  h4, p{
    margin: 0.5rem 0;
  }

  &:hover{
    background-color: ${
      props => props.theme ? props.theme.color.bg : 'transparent'
    }
  }
`

export default CardWrapper
export { CardWrapper }
