import React, { Component } from 'react'
import styled from 'styled-components'

import { Col } from '../container'

const CardRechargeTVWrapper = Col.extend`
	padding: 0;
	margin: 0.5rem;
	width: 118px;

	.card-border{
		border-radius: 2px;
		border: solid 1px #dbdbdb;
		height: 114px;
		padding: 0.6rem;
		width: 100%;
	}

	/* TODO: fix the color */
	&.disabled{
		/* opacity: 0.5; */
		* :not(.popover){
			color: ${ props => props.theme ? props.theme.colors.link : 'black' }
		}
	}
`

export default CardRechargeTVWrapper
export { CardRechargeTVWrapper }
