import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import '../../../config/styles/global-styles.js'

import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'
import { Header } from '../../organisms/Header'
import { Footer } from '../../organisms/Footer'

import { ContainerPlayer } from '../../organisms/ContainerPlayer'
import { ContainerCards } from '../../organisms/ContainerCards'

import { withTheme } from 'styled-components'


const OiPlayComp = (props) => {

  const {
    title,
    header ,
    subheader ,
    theme ,
    cards ,
    more ,
    hasEpisodes ,
    footerTitle,
    description,
    genre,
    rating
  } = props


  return (
    <Col sizeFull transparent>
      {/* header */}
      <Header title={header}/>

      {/* subheader */}
      <Header
        bg={theme.colors.secondary}
        type="subheader"
        title={subheader} />

      {/* video player */}
      <ContainerPlayer
        title={ title }
        genre={ genre }
        rating={ rating }
        description={ description }
        hasWatchLater = {true}
        watchLaterTxt = 'Assistir Depois'
        watchTraillerTxt = 'Assistir Trailler'
        seasontxt = 'Temporada TXT'/>

      {/* episodes */}
      { hasEpisodes ? <ContainerCards cards={cards}/> : null }

      {/* card more */}
      {/*TODO: implement more cards */}
      {/*<ContainerCards
        hasFooter
        cards={more}/>*/}

      {/* */}
      <Footer title={ footerTitle }/>
    </Col>
  )
}


OiPlayComp.defaultProps = {
  hasEpisodes : true
}

const OiPlayTemplate = withTheme(OiPlayComp)

export default OiPlayTemplate

export { OiPlayTemplate }
