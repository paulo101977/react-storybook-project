import React from 'react'
import { storiesOf } from '@storybook/react'
import { OiPlayTemplate } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

//import { serie } from '../../../mock'

import Immutable from 'immutable'

let child = {
	title: 'My title',
	mediumSynopsis: 'My Description',
	furnitureBaseUrl: '/assets/',
	furniture: [
		{uri: 'images/bg-hero.png'},
		{uri: 'images/bg-hero.png'},
		{uri: 'images/bg-hero.png'},
		{uri: 'images/bg-hero.png'}
	]
}

const makeChildren = () => {
	let children = []

	for(let i = 0; i < 10; i++){
		children.push(child)
	}

	return children
}

const serieObj = Immutable.fromJS({
	data: {
		longSynopsis: 'My Description',
		title: 'My Title',
		rating: '12',
		primaryGenre: ['Genre1', 'Genre2'],
		children: [{
				children: makeChildren()
		}]
	}
});


const data = serieObj.get("data")

const getCards = () => {
	return data.get('children').get(0).get('children')
}

//Example of how create new stories (storybook)
storiesOf('Templates', module)
	.add('OiPlayTemplate', () => (
		<ThemeProvider theme={ MinhaOi }>
			<OiPlayTemplate
				header="Oi Play"
				description={data.get('longSynopsis')}
				title={data.get('title')}
				genre={data.get('primaryGenre')}
				rating={data.get('rating')}
				cards={getCards()}
				subheader="Oi Play Subheader"
				footerTitle="Oi Play Footer"
				/>
		</ThemeProvider>
	));
