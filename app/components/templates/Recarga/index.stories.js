import React from 'react'
import { storiesOf } from '@storybook/react'
import { Recarga } from '.'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../../../styles/themes/MinhaOi'

import Immutable, { Map } from 'immutable'

const makePlanCards = () => {
	let card = {
		title:'Oi TV Start',
		channel:'127 canais',
		channelhd:'27 em HD',
		price:'R$ 27,99',
		btnmessage:'Lista de canais',
		selected:'',
		disabled: false
	},
	cards = []

	for(let i = 0; i < 10; i++){
		let obj = {}
		Object.assign(obj, card)

		if(i % 2 == 0) obj.disabled = true

		cards.push(obj)
	}

	return cards;
}

const itemsMaker = () => {
	return [
    {
      type: 'period',
      title: 'Escolha o período:',
      items: [
				"15 dias",
				"30 dias"
			]
    },
    {
      type: 'plan',
      title: 'Escolha o período:',
      items: makePlanCards()
    },
    {
      type: 'special',
      title: 'Escolha o período:',
      items: []
    }
  ]
}

const obj = Immutable.fromJS({
	header: "Recarga de TV Pré-paga",
	items: itemsMaker()
})

const period = Map({
	selectedTxt: 'test period'
})

const plan = Map({
	selectedTxt: 'test plan'
})

const special = Map({
	selectedTxt: 'test special'
})

//Example of how create new stories (storybook)
storiesOf('Templates', module)
	.add('Recarga', () => (
		<ThemeProvider theme={ MinhaOi }>
			<Recarga
				content={obj}
				period={period}
				plan={plan}
				special={special}
				/>
		</ThemeProvider>
	));
