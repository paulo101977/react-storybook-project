import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Immutable from 'immutable'

import '../../../config/styles/global-styles.js'

import  * as Title from '../../atoms/title'
import { Row, Col } from  '../../atoms/container'
import { Header } from '../../organisms/Header'
import { Footer } from '../../organisms/Footer'

import ContainerSelection from '../../organisms/ContainerSelection'

import { withTheme } from 'styled-components'


const RecargaComp = ({
  content,
  period,
  plan,
  special,
  updatePlan, //fn
  updatePeriod, //fn
  updateSpecial //fn
}) => {

  const header = content.get("header")
  const items = content.get("items")

  return (
    <Col sizeFull transparent>
      {/* header */}
      <Header
        hasLogo
        title={ header }/>

      <Row transparent container>
        <ContainerSelection
          items={ items }
          period={ period }
          plan={ plan }
          special={ special }
          updatePlan={ updatePlan }
          updatePeriod={ updatePeriod }
          updateSpecial={ updateSpecial }
          size3/>
        {/* TODO: other components */}
        {/*
          Example:
          <Form size3/>
          <Credicard size3/>
        */}
      </Row>

    </Col>
  )
}


RecargaComp.defaultProps = {
  header : ''
}


RecargaComp.defaultProps = {
  content: Immutable.fromJS({
    header: '',
    items: []
  })
}

const Recarga = withTheme(RecargaComp)

export default Recarga

export { Recarga }
