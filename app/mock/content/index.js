const makePlanCards = () => {
	let card = {
		title:'TV Start',
		channel:'127 canais',
		channelhd:'27 em HD',
		price:'R$ 27,99',
		btnmessage:'Lista de canais',
		selected:'',
		disabled: false
	},
	cards = []

	for(let i = 0; i < 10; i++){
		let obj = {}

		Object.assign(obj, card)

		if(i % 2 == 0) obj.disabled = true;
		else obj.disabled = false;
		cards.push(obj)
	}

	return cards;
}


module.exports = {
	content:{
	  header: "Recarga de TV Pré-paga",
	  disableds: [
	    false,
	    true,
	    true
	  ],
	  items: [
	    {
	      type: 'period',
	      title: 'Escolha o período:',
	      items: [
					"15 dias",
					"30 dias"
				]
	    },
	    {
	      type: 'plan',
	      title: 'Escolha o período:',
	      items: makePlanCards()
	    },
	    {
	      type: 'special',
	      title: 'Escolha o período:',
	      items: []
	    }
	  ]
	}
}
