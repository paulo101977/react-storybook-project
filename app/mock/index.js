
module.exports = {
  serie: require('./serie'),
  episodes: require('./episodes'),
  content: require('./content')
}
