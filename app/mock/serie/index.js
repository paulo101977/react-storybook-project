module.exports = {  
   "Data":{
      "ExternalIdentifiers":[
         {
            "Value":"SH026170950000",
            "Key":"TMSId"
         }
      ],
      "SupplierReference":"SH026170950000",
      "InternallyPlayable":false,
      "Classification":"Brand",
      "Title":"Preacher",
      "LongSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
      "MediumSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
      "ShortSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
      "ReleaseYear":2016,
      "PrimaryGenre":[
         "Drama",
         "Comedia",
         "Paranormal"
      ],
      "Children":[
         {
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950001",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950001",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Pilot",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1463875200,
                  "LongSynopsis":"Jesse luta para escapar de um passado que est\u00e1 lentamente se aproximando dele.",
                  "MediumSynopsis":"Jesse luta para escapar de um passado que est\u00e1 lentamente se aproximando dele.",
                  "ShortSynopsis":"Jesse luta para escapar de um passado que est\u00e1 lentamente se aproximando dele.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":64,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":1,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ],
                     "Furniture":[
                        {
                           "Uri":"assets\/p12713262_e_h10_ab.jpg",
                           "FurnitureType":"landscape2_1920x1080"
                        },
                        {
                           "Uri":"assets\/p12713262_e_h11_ab.jpg",
                           "FurnitureType":"landscape1_1280x720"
                        },
                        {
                           "Uri":"assets\/p12713262_e_v9_ab.jpg",
                           "FurnitureType":"portrait2_1080x1440"
                        },
                        {
                           "Uri":"assets\/p12713262_e_v7_ab.jpg",
                           "FurnitureType":"portrait1_480x720"
                        }
                     ],
                     "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12713262_e_h10_ab.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12713262_e_h11_ab.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12713262_e_v9_ab.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12713262_e_v7_ab.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950002",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950002",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"See",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1465084800,
                  "LongSynopsis":"Jesse tenta ser um \"bom pastor\", sem saber que uma dupla misteriosa est\u00e1 atr\u00e1s dele.",
                  "MediumSynopsis":"Jesse tenta ser um \"bom pastor\", sem saber que uma dupla misteriosa est\u00e1 atr\u00e1s dele.",
                  "ShortSynopsis":"Jesse tenta ser um \"bom pastor\", sem saber que uma dupla misteriosa est\u00e1 atr\u00e1s dele.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":49,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010123",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":2,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12834510_e_h10_ac.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12834510_e_h11_ac.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12834510_e_v9_ac.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12834510_e_v7_ac.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950003",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950003",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"The Possibilities",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1465689600,
                  "LongSynopsis":"Jesse explora seu novo poder com a ajuda de Cassidy.",
                  "MediumSynopsis":"Jesse explora seu novo poder com a ajuda de Cassidy.",
                  "ShortSynopsis":"Jesse explora seu novo poder com a ajuda de Cassidy.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":42,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010122",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":3,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12834879_e_h10_ac.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12834879_e_h11_ac.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12834879_e_v9_ac.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12834879_e_v7_ac.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950004",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950004",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Monster Swamp",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1466294400,
                  "LongSynopsis":"Jesse faz \u00e0 Quincannon uma aposta irrecus\u00e1vel.",
                  "MediumSynopsis":"Jesse faz \u00e0 Quincannon uma aposta irrecus\u00e1vel.",
                  "ShortSynopsis":"Jesse faz \u00e0 Quincannon uma aposta irrecus\u00e1vel.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":42,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010121",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":4,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12835118_e_h10_ac.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12835118_e_h11_ac.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12835118_e_v9_ac.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12835118_e_v7_ac.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950005",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950005",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"South Will Rise Again",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1466899200,
                  "LongSynopsis":"Depois de sua fa\u00e7anha com Quincannon, Jesse \u00e9 o mais novo rockstar de Annville.",
                  "MediumSynopsis":"Depois de sua fa\u00e7anha com Quincannon, Jesse \u00e9 o mais novo rockstar de Annville.",
                  "ShortSynopsis":"Depois de sua fa\u00e7anha com Quincannon, Jesse \u00e9 o mais novo rockstar de Annville.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":42,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010094",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":5,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12837187_e_h10_ab.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12837187_e_h11_ab.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12837187_e_v9_ab.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12837187_e_v7_ab.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950006",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950006",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Sundowner",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1467504000,
                  "LongSynopsis":"Jesse finalmente aprende sobre a misteriosa entidade que tomou conta de seu corpo.",
                  "MediumSynopsis":"Jesse finalmente aprende sobre a misteriosa entidade que tomou conta de seu corpo.",
                  "ShortSynopsis":"Jesse finalmente aprende sobre a misteriosa entidade que tomou conta de seu corpo.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":42,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010093",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":6,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12919009_e_h10_ab.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12919009_e_h11_ab.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12919009_e_v9_ab.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12919009_e_v7_ab.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950007",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950007",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"He Gone",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1468108800,
                  "LongSynopsis":"As atitudes de Jesse alienam e p\u00f5e em perigo as pessoas mais pr\u00f3ximas a ele.",
                  "MediumSynopsis":"As atitudes de Jesse alienam e p\u00f5e em perigo as pessoas mais pr\u00f3ximas a ele.",
                  "ShortSynopsis":"As atitudes de Jesse alienam e p\u00f5e em perigo as pessoas mais pr\u00f3ximas a ele.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":44,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010092",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":7,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12919190_e_h10_ab.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12919190_e_h11_ab.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12919190_e_v9_ab.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12919190_e_v7_ab.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950008",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950008",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"El Valero",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1468713600,
                  "LongSynopsis":"Jesse enfrenta Quincannon e os Homens de Carne para proteger sua igreja.",
                  "MediumSynopsis":"Jesse enfrenta Quincannon e os Homens de Carne para proteger sua igreja.",
                  "ShortSynopsis":"Jesse enfrenta Quincannon e os Homens de Carne para proteger sua igreja.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":42,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010131",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":8,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12919604_e_h10_ac.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12919604_e_h11_ac.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12919604_e_v9_ac.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12919604_e_v7_ac.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950009",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950009",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Finish the Song",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1469318400,
                  "LongSynopsis":"Jesse est\u00e1 sumido, enquanto as pessoas pr\u00f3ximas a ele enfrentam decis\u00f5es que podem mudar suas vidas.",
                  "MediumSynopsis":"Jesse est\u00e1 sumido, enquanto as pessoas pr\u00f3ximas a ele enfrentam decis\u00f5es que podem mudar suas vidas.",
                  "ShortSynopsis":"Jesse est\u00e1 sumido, enquanto as pessoas pr\u00f3ximas a ele enfrentam decis\u00f5es que podem mudar suas vidas.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":47,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010129",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":9,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12919824_e_h10_ac.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12919824_e_h11_ac.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12919824_e_v9_ac.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12919824_e_v7_ac.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950010",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950010",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Call and Response",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2016,
                  "SortCriteria":1469923200,
                  "LongSynopsis":"Jesse espera cumprir sua promessa de obter respostas do C\u00e9u.",
                  "MediumSynopsis":"Jesse espera cumprir sua promessa de obter respostas do C\u00e9u.",
                  "ShortSynopsis":"Jesse espera cumprir sua promessa de obter respostas do C\u00e9u.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":54,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010130",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":10,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S1",
                     "InternallyPlayable":false,
                     "NumberInSequence":1,
                     "Title":"Temporada 1",
                     "TitleKeywords":"temporada 1",
                     "Classification":"Season",
                     "SortTitle":"Temporada 1",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p12920002_e_h10_ab.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p12920002_e_h11_ab.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p12920002_e_v9_ab.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p12920002_e_v7_ab.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               }
            ],
            "Parent":{
               "ExternalIdentifiers":[
                  {
                     "Value":"SH026170950000",
                     "Key":"TMSId"
                  }
               ],
               "SupplierReference":"SH026170950000",
               "InternallyPlayable":false,
               "Classification":"Brand",
               "Title":"Preacher",
               "LongSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
               "MediumSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
               "ShortSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
               "ReleaseYear":2016,
               "PrimaryGenre":[
                  "Drama",
                  "Comedia",
                  "Paranormal"
               ],
               "Children":[

               ]
            },
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ],
            "Furniture":[
               {
                  "Uri":"assets\/p12713262_e_h10_ab.jpg",
                  "FurnitureType":"landscape2_1920x1080"
               },
               {
                  "Uri":"assets\/p12713262_e_h11_ab.jpg",
                  "FurnitureType":"landscape1_1280x720"
               },
               {
                  "Uri":"assets\/p12713262_e_v9_ab.jpg",
                  "FurnitureType":"portrait2_1080x1440"
               },
               {
                  "Uri":"assets\/p12713262_e_v7_ab.jpg",
                  "FurnitureType":"portrait1_480x720"
               }
            ],
            "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
         },
         {
            "SupplierReference":"SH026170950000-S2",
            "InternallyPlayable":false,
            "NumberInSequence":2,
            "Title":"Temporada 2",
            "TitleKeywords":"temporada 2",
            "Classification":"Season",
            "SortTitle":"Temporada 2",
            "Children":[
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950012",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950012",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"On the Road",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1498348800,
                  "LongSynopsis":"Jesse e seus amigos partem em busca de Deus, sem perceber que um inimigo mortal segue cada um de seus movimentos.",
                  "MediumSynopsis":"Jesse e seus amigos partem em busca de Deus, sem perceber que um inimigo mortal segue cada um de seus movimentos.",
                  "ShortSynopsis":"Jesse e seus amigos partem em busca de Deus, sem perceber que um inimigo mortal segue cada um de seus movimentos.",
                  "Certificate":"18",
                  "Rating":18,
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":44,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010814",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":1,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ],
                     "Furniture":[
                        {
                           "Uri":"assets\/p14083073_e_h10_aa.jpg",
                           "FurnitureType":"landscape2_1920x1080"
                        },
                        {
                           "Uri":"assets\/p14083073_e_h11_aa.jpg",
                           "FurnitureType":"landscape1_1280x720"
                        },
                        {
                           "Uri":"assets\/p14083073_e_v9_aa.jpg",
                           "FurnitureType":"portrait2_1080x1440"
                        },
                        {
                           "Uri":"assets\/p14083073_e_v7_aa.jpg",
                           "FurnitureType":"portrait1_480x720"
                        }
                     ],
                     "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14083073_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14083073_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14083073_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14083073_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950011",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950011",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Mumbai Sky Tower",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1498435200,
                  "LongSynopsis":"Um antigo parceiro de crime retorna para amea\u00e7ar Tulip; trio busca informa\u00e7\u00f5es sobre o cowboy.",
                  "MediumSynopsis":"Um antigo parceiro de crime retorna para amea\u00e7ar Tulip; trio busca informa\u00e7\u00f5es sobre o cowboy.",
                  "ShortSynopsis":"Um antigo parceiro de crime retorna para amea\u00e7ar Tulip; trio busca informa\u00e7\u00f5es sobre o cowboy.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":42,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":2,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14083081_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14083081_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14083081_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14083081_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950013",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950013",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Damsels",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1499040000,
                  "LongSynopsis":"Uma viagem a Nova Orleans amea\u00e7a expor Tulip a um ex-chefe zangado.",
                  "MediumSynopsis":"Uma viagem a Nova Orleans amea\u00e7a expor Tulip a um ex-chefe zangado.",
                  "ShortSynopsis":"Uma viagem a Nova Orleans amea\u00e7a expor Tulip a um ex-chefe zangado.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":45,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010817",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":3,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14113606_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14113606_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14113606_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14113606_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950014",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950014",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Viktor",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1499644800,
                  "LongSynopsis":"Jesse recebe not\u00edcias surpreendentes sobre sua busca por Deus.",
                  "MediumSynopsis":"Jesse recebe not\u00edcias surpreendentes sobre sua busca por Deus.",
                  "ShortSynopsis":"Jesse recebe not\u00edcias surpreendentes sobre sua busca por Deus.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":45,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010819",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":4,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14215936_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14215936_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14215936_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14215936_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950015",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950015",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Dallas",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1500249600,
                  "LongSynopsis":"Jesse busca vingan\u00e7a contra o marido mafioso de Tulip.",
                  "MediumSynopsis":"Jesse busca vingan\u00e7a contra o marido mafioso de Tulip.",
                  "ShortSynopsis":"Jesse busca vingan\u00e7a contra o marido mafioso de Tulip.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":40,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010823",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":5,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14215980_e_h10_ab.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14215980_e_h11_ab.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14215980_e_v9_ab.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14215980_e_v7_ab.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950016",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950016",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Sokosha",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1500854400,
                  "LongSynopsis":"Jesse entra em uma barganha arriscada com o Santo dos Assassinos.",
                  "MediumSynopsis":"Jesse entra em uma barganha arriscada com o Santo dos Assassinos.",
                  "ShortSynopsis":"Jesse entra em uma barganha arriscada com o Santo dos Assassinos.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":45,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010825",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":6,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14216038_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14216038_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14216038_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14216038_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950018",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950018",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Pig",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1501459200,
                  "LongSynopsis":"Cassidy encara uma decis\u00e3o importante relacionada a Denis.",
                  "MediumSynopsis":"Cassidy encara uma decis\u00e3o importante relacionada a Denis.",
                  "ShortSynopsis":"Cassidy encara uma decis\u00e3o importante relacionada a Denis.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":44,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010893",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":7,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14216070_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14216070_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14216070_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14216070_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950019",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950019",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Holes",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1502064000,
                  "LongSynopsis":"Jesse investiva uma pista sobre Deus; Tulip faz amizade com a nova vizinha; uma informa\u00e7\u00e3o sobre a rela\u00e7\u00e3o de Cassidy e Denis \u00e9 revelada.",
                  "MediumSynopsis":"Jesse investiva uma pista sobre Deus; Tulip faz amizade com a nova vizinha; uma informa\u00e7\u00e3o sobre a rela\u00e7\u00e3o de Cassidy e Denis \u00e9 revelada.",
                  "ShortSynopsis":"Jesse investiva uma pista sobre Deus; Tulip faz amizade com a nova vizinha; uma informa\u00e7\u00e3o sobre a rela\u00e7\u00e3o de Cassidy e Denis \u00e9 revelada.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":43,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010926",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":8,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14260284_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14260284_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14260284_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14260284_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950017",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950017",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Puzzle Piece",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1502668800,
                  "LongSynopsis":"Cassidy encara uma decis\u00e3o importante relacionada a Denis.",
                  "MediumSynopsis":"Cassidy encara uma decis\u00e3o importante relacionada a Denis.",
                  "ShortSynopsis":"Cassidy encara uma decis\u00e3o importante relacionada a Denis.",
                  "Certificate":"18",
                  "Rating":18,
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":42,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010927",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":9,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14260291_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14260291_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14260291_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14260291_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950021",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950021",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Dirty Little Secret",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1503273600,
                  "LongSynopsis":"Tulip suspeita que est\u00e1 sendo atra\u00edda para uma armadilha armada pela Graal.",
                  "MediumSynopsis":"Tulip suspeita que est\u00e1 sendo atra\u00edda para uma armadilha armada pela Graal.",
                  "ShortSynopsis":"Tulip suspeita que est\u00e1 sendo atra\u00edda para uma armadilha armada pela Graal.",
                  "Certificate":"18",
                  "Rating":18,
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":44,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8011015",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":10,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14260337_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14260337_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14260337_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14260337_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950020",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950020",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"Backdoors",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1503878400,
                  "LongSynopsis":"Jesse deve continuar sua busca por Deus sozinho.",
                  "MediumSynopsis":"Jesse deve continuar sua busca por Deus sozinho.",
                  "ShortSynopsis":"Jesse deve continuar sua busca por Deus sozinho.",
                  "Certificate":"18",
                  "Rating":18,
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":45,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8011018",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":11,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14260353_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14260353_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14260353_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14260353_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950022",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950022",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"On Your Knees",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1504483200,
                  "LongSynopsis":"Jesse, Tulip e Cassidy pegam a estrada e decidem sair de Nova Orleans; um inimigo reaparece.",
                  "MediumSynopsis":"Jesse, Tulip e Cassidy pegam a estrada e decidem sair de Nova Orleans; um inimigo reaparece.",
                  "ShortSynopsis":"Jesse, Tulip e Cassidy pegam a estrada e decidem sair de Nova Orleans; um inimigo reaparece.",
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":46,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8011166",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":12,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14260356_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14260356_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14260356_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14260356_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               },
               {
                  "ExternalIdentifiers":[
                     {
                        "Value":"EP026170950023",
                        "Key":"TMSId"
                     }
                  ],
                  "SupplierReference":"EP026170950023",
                  "InternallyPlayable":false,
                  "Classification":"Episode",
                  "Title":"The End of the Road",
                  "BrandTitle":"Preacher",
                  "ReleaseYear":2017,
                  "SortCriteria":1505088000,
                  "LongSynopsis":"Tulip descobre a verdade sobre o Graal.",
                  "MediumSynopsis":"Tulip descobre a verdade sobre o Graal.",
                  "ShortSynopsis":"Tulip descobre a verdade sobre o Graal.",
                  "Certificate":"18",
                  "Rating":18,
                  "PrimaryGenre":[
                     "Drama",
                     "Comedia",
                     "Paranormal"
                  ],
                  "Duration":54,
                  "ExternalContent":[
                     {
                        "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8011167",
                        "ProviderName":"Crackle BR",
                        "ProviderId":"92305",
                        "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                        "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                        "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                     }
                  ],
                  "NumberInSequence":13,
                  "Parent":{
                     "SupplierReference":"SH026170950000-S2",
                     "InternallyPlayable":false,
                     "NumberInSequence":2,
                     "Title":"Temporada 2",
                     "TitleKeywords":"temporada 2",
                     "Classification":"Season",
                     "SortTitle":"Temporada 2",
                     "Children":[

                     ],
                     "ExternalContent":[
                        {
                           "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                           "ProviderName":"Crackle BR",
                           "ProviderId":"92305",
                           "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                           "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                           "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                        }
                     ]
                  },
                  "Furniture":[
                     {
                        "Uri":"assets\/p14260362_e_h10_aa.jpg",
                        "FurnitureType":"landscape2_1920x1080"
                     },
                     {
                        "Uri":"assets\/p14260362_e_h11_aa.jpg",
                        "FurnitureType":"landscape1_1280x720"
                     },
                     {
                        "Uri":"assets\/p14260362_e_v9_aa.jpg",
                        "FurnitureType":"portrait2_1080x1440"
                     },
                     {
                        "Uri":"assets\/p14260362_e_v7_aa.jpg",
                        "FurnitureType":"portrait1_480x720"
                     }
                  ],
                  "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
               }
            ],
            "Parent":{
               "ExternalIdentifiers":[
                  {
                     "Value":"SH026170950000",
                     "Key":"TMSId"
                  }
               ],
               "SupplierReference":"SH026170950000",
               "InternallyPlayable":false,
               "Classification":"Brand",
               "Title":"Preacher",
               "LongSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
               "MediumSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
               "ShortSynopsis":"Jesse Custer, um ex-pastor do Texas, acaba recebendo o poder de fazer com que qualquer um o obede\u00e7a. Acompanhado de sua ex-namorada Tulip e do vampiro irland\u00eas Cassidy, Jesse vai atr\u00e1s de Deus, que abandonou o para\u00edso.",
               "ReleaseYear":2016,
               "PrimaryGenre":[
                  "Drama",
                  "Comedia",
                  "Paranormal"
               ],
               "Children":[

               ],
               "ExternalContent":[
                  {
                     "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                     "ProviderName":"Crackle BR",
                     "ProviderId":"92305",
                     "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                     "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                     "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
                  }
               ]
            },
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010813",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ],
            "Furniture":[
               {
                  "Uri":"assets\/p14083073_e_h10_aa.jpg",
                  "FurnitureType":"landscape2_1920x1080"
               },
               {
                  "Uri":"assets\/p14083073_e_h11_aa.jpg",
                  "FurnitureType":"landscape1_1280x720"
               },
               {
                  "Uri":"assets\/p14083073_e_v9_aa.jpg",
                  "FurnitureType":"portrait2_1080x1440"
               },
               {
                  "Uri":"assets\/p14083073_e_v7_aa.jpg",
                  "FurnitureType":"portrait1_480x720"
               }
            ],
            "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
         }
      ],
      "ExternalContent":[
         {
            "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
            "ProviderName":"Crackle BR",
            "ProviderId":"92305",
            "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
            "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
            "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
         }
      ],
      "Furniture":[
         {
            "Uri":"assets\/p12713256_i_s4_aa.jpg",
            "FurnitureType":"portrait2_3000x3000"
         },
         {
            "Uri":"assets\/p12713261_b_v7_aa.jpg",
            "FurnitureType":"portrait1_480x720"
         },
         {
            "Uri":"assets\/p12713256_i_h10_ab.jpg",
            "FurnitureType":"landscape2_1920x1080"
         },
         {
            "Uri":"assets\/p12713256_i_h11_ab.jpg",
            "FurnitureType":"landscape1_1280x720"
         }
      ],
      "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
   }
}
