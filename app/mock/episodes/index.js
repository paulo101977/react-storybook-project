module.exports = {
   "Data":[
      {
         "ExternalIdentifiers":[
            {  
               "Value":"EP026170950001",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950001",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"Pilot",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1463875200,
         "LongSynopsis":"Jesse luta para escapar de um passado que est\u00e1 lentamente se aproximando dele.",
         "MediumSynopsis":"Jesse luta para escapar de um passado que est\u00e1 lentamente se aproximando dele.",
         "ShortSynopsis":"Jesse luta para escapar de um passado que est\u00e1 lentamente se aproximando dele.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":64,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":1,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ],
            "Furniture":[
               {
                  "Uri":"assets\/p12713262_e_h10_ab.jpg",
                  "FurnitureType":"landscape2_1920x1080"
               },
               {
                  "Uri":"assets\/p12713262_e_h11_ab.jpg",
                  "FurnitureType":"landscape1_1280x720"
               },
               {
                  "Uri":"assets\/p12713262_e_v9_ab.jpg",
                  "FurnitureType":"portrait2_1080x1440"
               },
               {
                  "Uri":"assets\/p12713262_e_v7_ab.jpg",
                  "FurnitureType":"portrait1_480x720"
               }
            ],
            "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
         },
         "Furniture":[
            {
               "Uri":"assets\/p12713262_e_h10_ab.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12713262_e_h11_ab.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12713262_e_v9_ab.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12713262_e_v7_ab.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950002",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950002",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"See",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1465084800,
         "LongSynopsis":"Jesse tenta ser um \"bom pastor\", sem saber que uma dupla misteriosa est\u00e1 atr\u00e1s dele.",
         "MediumSynopsis":"Jesse tenta ser um \"bom pastor\", sem saber que uma dupla misteriosa est\u00e1 atr\u00e1s dele.",
         "ShortSynopsis":"Jesse tenta ser um \"bom pastor\", sem saber que uma dupla misteriosa est\u00e1 atr\u00e1s dele.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":49,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010123",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":2,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12834510_e_h10_ac.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12834510_e_h11_ac.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12834510_e_v9_ac.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12834510_e_v7_ac.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950003",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950003",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"The Possibilities",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1465689600,
         "LongSynopsis":"Jesse explora seu novo poder com a ajuda de Cassidy.",
         "MediumSynopsis":"Jesse explora seu novo poder com a ajuda de Cassidy.",
         "ShortSynopsis":"Jesse explora seu novo poder com a ajuda de Cassidy.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":42,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010122",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":3,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12834879_e_h10_ac.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12834879_e_h11_ac.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12834879_e_v9_ac.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12834879_e_v7_ac.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950004",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950004",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"Monster Swamp",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1466294400,
         "LongSynopsis":"Jesse faz \u00e0 Quincannon uma aposta irrecus\u00e1vel.",
         "MediumSynopsis":"Jesse faz \u00e0 Quincannon uma aposta irrecus\u00e1vel.",
         "ShortSynopsis":"Jesse faz \u00e0 Quincannon uma aposta irrecus\u00e1vel.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":42,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010121",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":4,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12835118_e_h10_ac.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12835118_e_h11_ac.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12835118_e_v9_ac.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12835118_e_v7_ac.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950005",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950005",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"South Will Rise Again",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1466899200,
         "LongSynopsis":"Depois de sua fa\u00e7anha com Quincannon, Jesse \u00e9 o mais novo rockstar de Annville.",
         "MediumSynopsis":"Depois de sua fa\u00e7anha com Quincannon, Jesse \u00e9 o mais novo rockstar de Annville.",
         "ShortSynopsis":"Depois de sua fa\u00e7anha com Quincannon, Jesse \u00e9 o mais novo rockstar de Annville.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":42,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010094",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":5,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12837187_e_h10_ab.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12837187_e_h11_ab.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12837187_e_v9_ab.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12837187_e_v7_ab.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950006",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950006",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"Sundowner",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1467504000,
         "LongSynopsis":"Jesse finalmente aprende sobre a misteriosa entidade que tomou conta de seu corpo.",
         "MediumSynopsis":"Jesse finalmente aprende sobre a misteriosa entidade que tomou conta de seu corpo.",
         "ShortSynopsis":"Jesse finalmente aprende sobre a misteriosa entidade que tomou conta de seu corpo.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":42,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010093",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":6,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12919009_e_h10_ab.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12919009_e_h11_ab.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12919009_e_v9_ab.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12919009_e_v7_ab.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950007",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950007",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"He Gone",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1468108800,
         "LongSynopsis":"As atitudes de Jesse alienam e p\u00f5e em perigo as pessoas mais pr\u00f3ximas a ele.",
         "MediumSynopsis":"As atitudes de Jesse alienam e p\u00f5e em perigo as pessoas mais pr\u00f3ximas a ele.",
         "ShortSynopsis":"As atitudes de Jesse alienam e p\u00f5e em perigo as pessoas mais pr\u00f3ximas a ele.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":44,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010092",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":7,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12919190_e_h10_ab.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12919190_e_h11_ab.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12919190_e_v9_ab.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12919190_e_v7_ab.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950008",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950008",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"El Valero",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1468713600,
         "LongSynopsis":"Jesse enfrenta Quincannon e os Homens de Carne para proteger sua igreja.",
         "MediumSynopsis":"Jesse enfrenta Quincannon e os Homens de Carne para proteger sua igreja.",
         "ShortSynopsis":"Jesse enfrenta Quincannon e os Homens de Carne para proteger sua igreja.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":42,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010131",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":8,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12919604_e_h10_ac.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12919604_e_h11_ac.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12919604_e_v9_ac.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12919604_e_v7_ac.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950009",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950009",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"Finish the Song",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1469318400,
         "LongSynopsis":"Jesse est\u00e1 sumido, enquanto as pessoas pr\u00f3ximas a ele enfrentam decis\u00f5es que podem mudar suas vidas.",
         "MediumSynopsis":"Jesse est\u00e1 sumido, enquanto as pessoas pr\u00f3ximas a ele enfrentam decis\u00f5es que podem mudar suas vidas.",
         "ShortSynopsis":"Jesse est\u00e1 sumido, enquanto as pessoas pr\u00f3ximas a ele enfrentam decis\u00f5es que podem mudar suas vidas.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":47,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010129",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":9,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12919824_e_h10_ac.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12919824_e_h11_ac.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12919824_e_v9_ac.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12919824_e_v7_ac.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      },
      {
         "ExternalIdentifiers":[
            {
               "Value":"EP026170950010",
               "Key":"TMSId"
            }
         ],
         "SupplierReference":"EP026170950010",
         "InternallyPlayable":false,
         "Classification":"Episode",
         "Title":"Call and Response",
         "BrandTitle":"Preacher",
         "ReleaseYear":2016,
         "SortCriteria":1469923200,
         "LongSynopsis":"Jesse espera cumprir sua promessa de obter respostas do C\u00e9u.",
         "MediumSynopsis":"Jesse espera cumprir sua promessa de obter respostas do C\u00e9u.",
         "ShortSynopsis":"Jesse espera cumprir sua promessa de obter respostas do C\u00e9u.",
         "PrimaryGenre":[
            "Drama",
            "Comedia",
            "Paranormal"
         ],
         "Duration":54,
         "ExternalContent":[
            {
               "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010130",
               "ProviderName":"Crackle BR",
               "ProviderId":"92305",
               "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
               "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
               "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
            }
         ],
         "NumberInSequence":10,
         "Parent":{
            "SupplierReference":"SH026170950000-S1",
            "InternallyPlayable":false,
            "NumberInSequence":1,
            "Title":"Temporada 1",
            "TitleKeywords":"temporada 1",
            "Classification":"Season",
            "SortTitle":"Temporada 1",
            "Children":[

            ],
            "ExternalContent":[
               {
                  "ContentUrl":"https:\/\/www.crackle.com\/preacher\/8010120",
                  "ProviderName":"Crackle BR",
                  "ProviderId":"92305",
                  "ProviderLogoUrl":"https:\/\/apinew-cr-oi-prod-bs.sf.vubiquity.com\/uploads\/media\/default\/0001\/01\/2b61a42067a33932582bcfa30c015480938583e7.png",
                  "SecondaryProviderLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png",
                  "ChannelLogoUrl":"https:\/\/oiplay.s3.amazonaws.com\/channels\/4\/92305.png"
               }
            ]
         },
         "Furniture":[
            {
               "Uri":"assets\/p12920002_e_h10_ab.jpg",
               "FurnitureType":"landscape2_1920x1080"
            },
            {
               "Uri":"assets\/p12920002_e_h11_ab.jpg",
               "FurnitureType":"landscape1_1280x720"
            },
            {
               "Uri":"assets\/p12920002_e_v9_ab.jpg",
               "FurnitureType":"portrait2_1080x1440"
            },
            {
               "Uri":"assets\/p12920002_e_v7_ab.jpg",
               "FurnitureType":"portrait1_480x720"
            }
         ],
         "FurnitureBaseUrl":"http:\/\/oisa.tmsimg.com\/"
      }
   ]
}
