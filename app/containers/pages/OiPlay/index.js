import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as LoadData from '../../../actions/LoadData';
import {loadData} from '../../../actions/LoadData';
import Immutable from 'immutable';

import OiPlayTemplate from '../../../components/templates/OiPlayTemplate'
import { Col } from '../../../components/atoms/container'

import Helmet from 'react-helmet'


class OiPlay extends Component {
  constructor(props){
    super(props)
  }

  static fetchData({store}){
    return store.dispatch(loadData())
  }

  componentDidMount() {
    this.props.loadData()
  }

  getCards(data){

    return data.get('children').get(0).get('children')

  }

  render() {
    const { data } = this.props;
    const longSynopsis = data.get('longSynopsis')
    const title = data.get('title')
    const rating = data.get('rating')
    const primaryGenre = data.get('primaryGenre')

    return (
      <Col transparent>
        {/* meta tags */}
        <Helmet>
            {/* the page encode */}
            <meta charSet="utf-8" />

            {/* the page title */}
            <title>My Title</title>

            {/* other meta tags */}

            <meta name="description" content="Oi Play Home Page" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1.0" />
        </Helmet>

        {/* content */}
        <OiPlayTemplate {...this.props}
          header="Oi Play"
          description={ longSynopsis }
          title={ title }
          genre={ primaryGenre }
          rating={ rating }
          cards={this.getCards(data)}
          subheader="Oi Play Subheader"
          footerTitle="Oi Play Footer"
          />
      </Col>
    )
  }
}


function mapStateToProps (state) {
  const response = state.LoadData.get('response')

  // data from immutable (immutable way)
  let data = response.get("data")

  return {
    data
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    LoadData,
    dispatch
  )
}

OiPlay.defaultProps = {
  data: Immutable.fromJS({}),
  response: {}
}

export default connect(mapStateToProps, mapDispatchToProps)(OiPlay)

export { OiPlay }
