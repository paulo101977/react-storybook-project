import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import Recarga from '../../../components/templates/Recarga'
import { Col } from '../../../components/atoms/container'

import * as LoadContent from '../../../actions/LoadContent'
import { loadContent } from '../../../actions/LoadContent'
import * as SelectionChange from '../../../actions/SelectionChange'

import Helmet from 'react-helmet'

class RecargaTV extends Component {
  constructor(props){
    super(props)
  }

  static fetchData({store}){
    return store.dispatch(loadContent())
  }

  componentDidMount() {
    this.props.loadContent()
  }


  render() {

    return (
      <Col transparent>
        {/* meta tags */}
        <Helmet>
            {/* the page encode */}
            <meta charSet="utf-8" />

            {/* the page title */}
            <title>My Title</title>

            {/* other meta tags */}

            <meta name="description" content="Home Page" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1.0" />
        </Helmet>

        {/* content */}
        <Recarga {...this.props} />
      </Col>
    )
  }
}


function mapStateToProps (state) {
  const content = state.LoadContent.get("content")
  const period = state.SelectionChange.get('period')
	const plan = state.SelectionChange.get('plan')
	const special = state.SelectionChange.get('special')

  return {
    content,
    period,
    plan,
    special
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...LoadContent,
      ...SelectionChange
    },
    dispatch
  )
}

RecargaTV.defaultProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(RecargaTV)

export { RecargaTV }
