import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Helmet from 'react-helmet'
import { Link } from 'react-router'

import { Col } from 'components/atoms/container'

import { ThemeProvider } from 'styled-components'

import { MinhaOi } from '../styles/themes/MinhaOi'

class App extends Component {
  render() {
  	let { App } = this.props

    return (
      <ThemeProvider theme={ MinhaOi }>
        <Col transparent='true'>
          { this.props.children }
        </Col>
      </ThemeProvider>
    )
  }
}


export default App
