## Settings:

API_BASE is the root application (eg.: localhost:8080/faq)
API_BASE_URL_EXTERNAL is the external api request base
API_BASE_URL_INTERNAL is the internal api request base

NOTE: The API_BASE should have the format `/${path}/` 

## Folders

```
.
├── .storybook      # storybook config folder
|
├── app             # main folder                 
│   ├── actions     # put your actions
│   │                  ...
│   ├── assets      # put your assets
│   │                  ...
│   └── components  # your atomic design components
│   │                  ...
│   └── config      # general config
│   │                  ...
│   └── containers  # your pages
│   │                  ...
│   └── middleware  # redux middleware
│   │                  ...
│   └── mock        # mock data folder
│   │                  ...
│   └── routes      # general routes config
│   │                  ...
│   └── server      # general server config
│   │                  ...
│   └── store       # redux store
│   │                  ...
│   └── styles      # themes and others
│   │                  ...
│   index.js        # client app
│  
└── dist            # the generated dist builded
│  
└── tests            # API tests
│
└── stories         # the storybook stories general config
```

## Run production ##
npm run build && npm run prod

go to  http://localhost:8080

## install ##
npm install

## Start dev ##
npm start
-
go to  http://localhost:3000

## Storybook ##
npm run storybook
-
go to  http://localhost:6006
